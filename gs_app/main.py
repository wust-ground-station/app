from fastapi import FastAPI, Request, Cookie
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel

from gs_app.definitions import TEMPLATES_FOLDER, STATIC_FOLDER

app = FastAPI()
app.mount("/static", StaticFiles(directory=STATIC_FOLDER), name="static")

templates = Jinja2Templates(directory=TEMPLATES_FOLDER)

@app.get("/login/", response_class=HTMLResponse)
def login_get(
        request: Request,
        access_token: str = Cookie(None)
    ):
    context = {
        "request": request,
        "title": "Sing in"
    }
    return templates.TemplateResponse("login.html", context)

@app.get('/1', response_class=HTMLResponse)
def index(request: Request):
    context = {'request': request}
    return templates.TemplateResponse("index.html", context)

@app.get('/2', response_class=HTMLResponse)
def index(request: Request):
    context = {'request': request}
    return templates.TemplateResponse("index2.html", context)

@app.get('/3', response_class=HTMLResponse)
def index(request: Request):
    context = {'request': request}
    return templates.TemplateResponse("index3.html", context)

@app.get('/4', response_class=HTMLResponse)
def index(request: Request):
    context = {'request': request}
    return templates.TemplateResponse("index4.html", context)

class ExampleForm(BaseModel):
    message: str

@app.post("/save")
async def save_message(data: ExampleForm):
    message = data.message
    with open("messages.txt", "a") as file:
        file.write(message + "\n")
    return "Message written to file"
