from pathlib import Path

GS_APP_FOLDER = Path(__file__).parents[0]
GIT_REPO_FOLDER: Path = GS_APP_FOLDER.parents[0]
TEMPLATES_FOLDER: Path = GIT_REPO_FOLDER / "templates"
STATIC_FOLDER: Path = GIT_REPO_FOLDER / "static"
